# -*- coding: utf-8 -*-

from flask import Blueprint, jsonify, request, make_response, session
from werkzeug.security import check_password_hash
import jwt
import datetime
import os

from ..db import db
from ..utils import generateOTP, sendOTP

blueprint = Blueprint('auth', __name__)
Users = db.users


@blueprint.route('/login')
def login():
    data = request.get_json()

    if not data or not data["name"] or not data["password"]:
        return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    user = Users.find_one({'name': data["name"]})
    print(user)

    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    if check_password_hash(user["password"], data["password"]):
        token = jwt.encode({'public_id': user["public_id"], 'exp': datetime.datetime.utcnow() +
                            datetime.timedelta(hours=24)}, os.environ['SECRET_KEY'])

        return jsonify({'token': token.decode('UTF-8')})

    return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})


@blueprint.route('/otp')
def send_otp():
    data = request.get_json()
    userNumber = str(data['number'])

    otp = generateOTP()
    session[userNumber] = otp
    sendOTP(userNumber, otp)
    return jsonify({'message': 'otp sent to number'})


@blueprint.route('/verify')
def verify_otp():
    data = request.get_json()
    rec_otp = data['otp']
    user_number = str(data['number'])

    if user_number in session:
        stored_otp = session[user_number]
        session.pop(user_number, None)
        if stored_otp == rec_otp:
            return jsonify({'message': 'otp verified'})

    return jsonify({'message': 'otp invalid'})
