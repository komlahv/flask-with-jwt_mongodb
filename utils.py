from flask import session, request
import requests
import random


def sendOTP(number, otp):
    endPoint = 'https://api.mnotify.com/api/sms/quick'
    apiKey = 'apikey here'
    data = {
        'recipient[]': [number],
        'sender': 'Sender Name',
        'message': 'Your OTP is ' + otp,
        'is_schedule': False,
        'schedule_date': ''
    }
    url = endPoint + '?key=' + apiKey
    response = requests.post(url, data)
    data = response.json()
    print(data)


def generateOTP():
    return str(random.randrange(1000, 9999))


def validateOTP():
    data = request.get_json()
    recOTP = data['otp']

    if 'username' in session:
        storedOTP = session['username']
        session.pop('username', None)
        if storedOTP == recOTP:
            return True
