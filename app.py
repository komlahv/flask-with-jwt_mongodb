from flask import Flask
from flask_cors import CORS
from . import user
from . import auth
import os


app = Flask(__name__)
cors = CORS(app)

# do not use this in production
os.environ['SECRET_KEY'] = 'thisissecret'

# secret key for our session
app.secret_key = os.environ['SECRET_KEY']


app.register_blueprint(user.views.blueprint)
app.register_blueprint(auth.views.blueprint)


if __name__ == '__main__':
    app.run(debug=True)
