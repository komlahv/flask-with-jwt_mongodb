# -*- coding: utf-8 -*-

from flask import Blueprint, jsonify, request
from werkzeug.security import generate_password_hash
import uuid
from ..jwt_handler import token_required
from ..db import db

blueprint = Blueprint('user', __name__)
Users = db.users


@blueprint.route('/user', methods=['GET'])
@token_required
def get_all_users(current_user):

    if not current_user["admin"]:
        return jsonify({'message': 'Cannot perform that function!'})

    all_users = []
    for data in Users.find({}, {"_id": 0, "password": 0}):
        all_users.append(data)

    return jsonify(all_users)


@blueprint.route('/user/<public_id>', methods=['GET'])
@token_required
def get_one_user(current_user, public_id):

    if not current_user["admin"]:
        return jsonify({'message': 'Cannot perform that function!'})

    user = Users.find_one({'public_id': public_id}, {"_id": 0, "password": 0})

    if not user:
        return jsonify({'message': 'No user found!'})

    return user


@blueprint.route('/user', methods=['POST'])
def create_user():
    data = request.get_json()

    hashed_password = generate_password_hash(data['password'], method='sha256')

    new_user = Users.insert({
        'public_id': str(uuid.uuid4()),
        'name': data['name'],
        'password': hashed_password,
        'admin': False
    })

    print(new_user)
    return jsonify({'message': 'New user created!'})


@blueprint.route('/user/<public_id>', methods=['PUT'])
@token_required
def promote_user(current_user, public_id):
    if not current_user["admin"]:
        return jsonify({'message': 'Cannot perform that function!'})

    user = Users.update_one({'public_id': public_id}, {"$set": {'admin': True}})

    if not user:
        return jsonify({'message': 'No user found!'})

    return jsonify({'message': 'The user has been promoted!'})


@blueprint.route('/user/<public_id>', methods=['DELETE'])
@token_required
def delete_user(current_user, public_id):
    if not current_user["admin"]:
        return jsonify({'message': 'Cannot perform that function!'})

    user = Users.delete_one({'public_id': public_id})

    if not user:
        return jsonify({'message': 'No user found!'})

    return jsonify({'message': 'The user has been deleted!'})
